require "socket"

port = 8085
server  = TCPServer.new("localhost", port)

while session = server.accept()
	rand_number = rand(1..100).to_s()
	message = '{"random":' + rand_number + '}'
	body_length = message.length().to_s()
	session.print("HTTP/1.1 200 OK\r\n")
	session.print("Content-Type: application/json\r\n")
	session.print("Content-Length: " + body_length + "\r\n")
	session.print("\r\n")
	session.print(message)

	session.close()
end
